package schema

type MetadataFile struct {
	FileID      int      `json:"file_id"`
	OoyalaID    int      `json:"ooyala_id"`
	FileName    string   `json:"file_name"`
	LocalPath   string   `json:"lacal_path"`
	Size        int      `json:"size"`
	ReleaseAt   int      `json:"releast_at"`
	PublishAt   int      `json:"publish_at"`
	TotalBlocks int      `json:"total_blocks"`
	Checksum    string   `json:"checksum"`
	ExecScripts []string `json:"exec_scripts"`
}
