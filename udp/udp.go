package udp

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

type UDPClient struct {
	socket      string
	subblockDir string
}

func NewUDPClient(socket, subblockDir string) *UDPClient {
	return &UDPClient{
		socket:      socket,
		subblockDir: subblockDir,
	}
}

func (u *UDPClient) SendBlocks() error {
	raddr, rerr := net.ResolveUDPAddr("udp4", u.socket)
	if rerr != nil {
		return errors.Wrapf(rerr, "Failed while resolving socket: %s", u.socket)
	}

	conn, derr := net.DialUDP("udp4", nil, raddr)
	if derr != nil {
		return errors.Wrapf(derr, "Failed while dialing to udp client: %s", raddr)
	}

	fmt.Printf("The UDP client is %s\n", conn.RemoteAddr().String())
	defer conn.Close()

	reader := bufio.NewReader(os.Stdin)
	fileReg, _ := regexp.Compile(`^\d+$`)
	blockReg, _ := regexp.Compile(`^\d+:\d+$`)

	for {
		fmt.Print("fileID / fileID:blockID >> ")
		text, _ := reader.ReadString('\n')
		text = strings.TrimSuffix(text, "\n")

		if fileReg.MatchString(text) {
			fileID := text
			fmt.Printf("Sending subblocks of file <%s>\n", fileID)
			blockFolder := u.subblockDir + "/" + fileID

			blockEntries, derr := os.ReadDir(blockFolder)
			if derr != nil {
				fmt.Println("Failed reading dir. err:", derr)
				continue
			}

			for _, blockEntry := range blockEntries {
				if !blockEntry.IsDir() {
					fmt.Println("block entry is not a dir")
					continue
				}

				subblockFolder := blockFolder + "/" + blockEntry.Name()

				if serr := u.sendSubblocks(conn, subblockFolder); serr != nil {
					return serr
				}
			}
		} else if blockReg.MatchString(text) {
			metas := strings.Split(text, ":")

			fileID := metas[0]
			blockID := metas[1]
			fmt.Printf("Sending subblocks of block <%s> of file <%s>\n", blockID, fileID)

			subblockFolder := u.subblockDir + "/" + fileID + "/" + blockID

			if serr := u.sendSubblocks(conn, subblockFolder); serr != nil {
				return serr
			}
		}
	}
}

func (u *UDPClient) sendSubblocks(conn *net.UDPConn, subblockFolder string) error {
	subblockEntries, derr := os.ReadDir(subblockFolder)
	if derr != nil {
		fmt.Println("Failed reading dir. err:", derr)
	}

	for _, subblockEntry := range subblockEntries {
		data, rerr := ioutil.ReadFile(subblockFolder + "/" + subblockEntry.Name())
		if rerr != nil {
			fmt.Println(rerr)
			continue
		}

		if _, werr := conn.Write(data); werr != nil {
			return errors.Wrap(werr, "Failed while writing data")
		}
	}

	return nil
}
