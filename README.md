# block-receiver-local-test

go run main.go --files="Spheres.jpg:56 MountRainier:25"    // file name : block chunk number

go run main.go --blocks="3:4 4:5"    // file ID : subblock chunk number

go run main.go --udp_socket="224.0.0.1:3001"
fileID / fileID:blockID >> 4
fileID / fileID:blockID >> 4:3

go run main.go --tcp_socket="127.0.0.1:3000"
fileID >> 4