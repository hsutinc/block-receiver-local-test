package tcp

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"

	"github.com/pkg/errors"
)

type TCPClient struct {
	socket               string
	preprocessedBlockDir string
}

func NewTCPClient(socket, preprocessedBLockDir string) *TCPClient {
	return &TCPClient{
		socket:               socket,
		preprocessedBlockDir: preprocessedBLockDir,
	}
}

func (t *TCPClient) SendBlocks() error {
	conn, derr := net.Dial("tcp", t.socket)
	if derr != nil {
		return errors.Wrap(derr, "Failed diling")
	}

	remoteAddr := conn.RemoteAddr().String()

	// go func(c net.Conn) {
	// 	for {
	// 		message, _ := bufio.NewReader(c).ReadString('\n')
	// 		fmt.Print("->: " + message)
	// 		if message == "Connection timeout.\n" {
	// 			c.Write([]byte(" "))
	// 			c.Close()
	// 			return
	// 		}
	// 	}
	// }(conn)

	ackPacketSize := 1024
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("fileID >> ")
		fileID, _ := reader.ReadString('\n')

		processedBlockFolder := t.preprocessedBlockDir + "/" + strings.TrimSuffix(fileID, "\n")

		blockEntries, derr := os.ReadDir(processedBlockFolder)
		if derr != nil {
			fmt.Println("Failed reading dir. err:", derr)
			continue
		}

		for _, blockEntry := range blockEntries {
			if blockEntry.IsDir() {
				continue
			}

			data, rerr := ioutil.ReadFile(processedBlockFolder + "/" + blockEntry.Name())
			if rerr != nil {
				fmt.Println(rerr)
				continue
			}

			fmt.Println("Sending block with size:", len(data))
			if _, werr := conn.Write(data); werr != nil {
				return errors.Wrap(werr, "Failed while writing data")
			}

			buffer := make([]byte, ackPacketSize)
			rn, cerr := conn.Read(buffer)
			if cerr != nil {
				return errors.Wrap(cerr, fmt.Sprintf("failed to receive data. (fromAddr=%s)", remoteAddr))
			}

			fmt.Printf("got ack packet. (fromAddr=%s, ack=%s)\n", remoteAddr, buffer[:rn])
		}
	}
}
