package util

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
	"strconv"
)

func Chunk(file *os.File, chunkNum int, destFolder, msg string) {
	fileInfo, _ := file.Stat()

	fileSize := fileInfo.Size()
	fmt.Printf("File size: %d\n", fileSize)
	chunkSize := int64(math.Ceil(float64(fileSize) / float64(chunkNum)))

	fmt.Printf("Splitting %d byte size %s to %d chunks with size %d byte\n", fileSize, msg, chunkNum, chunkSize)

	// blockFolder := blockDir + "/" + fmt.Sprint(fileID)
	if merr := os.Mkdir(destFolder, 0777); merr != nil {
		fmt.Println(merr)
	}

	for i := 1; i <= chunkNum; i++ {
		var actualChunkSize int64
		if fileSize-int64((i-1)*int(chunkSize)) < chunkSize {
			actualChunkSize = fileSize - int64((i-1)*int(chunkSize))
		} else {
			actualChunkSize = chunkSize
		}

		buf := make([]byte, actualChunkSize)
		bytesRead, err := file.Read(buf)

		if err != nil {
			switch err {
			case io.EOF:
				fmt.Println("Done reading file.")
			default:
				fmt.Println(err)
			}
			break
		}

		blockFileName := destFolder + "/" + strconv.Itoa(i)
		_, werr := os.Create(blockFileName)

		if werr != nil {
			fmt.Println(werr)
			os.Exit(1)
		}

		// write/save buffer to disk
		ioutil.WriteFile(blockFileName, buf, os.ModeAppend)

		fmt.Println("bytes read: ", bytesRead)
		// fmt.Println("bytestream to string: ", string(buf[:bytesRead]))
	}
}

func ChunkData(data []byte, chunkNum int /*destFolder,*/, msg string) [][]byte {
	dataSize := len(data)
	fmt.Printf("File size: %d\n", dataSize)
	chunkSize := int64(math.Ceil(float64(dataSize) / float64(chunkNum)))

	fmt.Printf("Splitting %d byte size %s to %d chunks with size %d byte\n", dataSize, msg, chunkNum, chunkSize)

	// blockFolder := blockDir + "/" + fmt.Sprint(fileID)
	/*
		if merr := os.Mkdir(destFolder, 0777); merr != nil {
			fmt.Println(merr)
		}
	*/

	dataSlice := make([][]byte, chunkNum)
	var bytesRead int64

	for i := 1; i <= chunkNum; i++ {
		var actualChunkSize int64
		if int64(dataSize)-int64((i-1)*int(chunkSize)) < chunkSize {
			actualChunkSize = int64(dataSize) - int64((i-1)*int(chunkSize))
		} else {
			actualChunkSize = chunkSize
		}

		// buf := make([]byte, actualChunkSize)
		// bytesRead, err := file.Read(buf)

		// if err != nil {
		// 	switch err {
		// 	case io.EOF:
		// 		fmt.Println("Done reading file.")
		// 	default:
		// 		fmt.Println(err)
		// 	}
		// 	break
		// }

		buf := data[bytesRead : bytesRead+actualChunkSize]
		bytesRead += actualChunkSize

		dataSlice[i-1] = make([]byte, actualChunkSize)
		copy(dataSlice[i-1], buf)
		/*
			blockFileName := destFolder + "/" + strconv.Itoa(i)
			_, werr := os.Create(blockFileName)

			if werr != nil {
				fmt.Println(werr)
				os.Exit(1)
			}

			// write/save buffer to disk
			ioutil.WriteFile(blockFileName, buf, os.ModeAppend)
		*/

		fmt.Println("bytes read: ", bytesRead)
		// fmt.Println("bytestream to string: ", string(buf[:bytesRead]))
	}

	return dataSlice
}

func Encrypt(plaintext []byte) []byte {
	// The key argument should be the AES key, either 16 or 32 bytes
	// to select AES-128 or AES-256.
	// key := []byte(keystring)
	key := []byte("asdfljksdlfjaslsadfcvadfjasdfsdf")
	// fmt.Print("Encrypter's key: ")
	// fmt.Println(key)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}

	// Never use more than 2^32 random nonces with a given key because of the risk of a repeat.
	nonce := make([]byte, 12)
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	dst := aesgcm.Seal(nil, nonce, plaintext, nil)
	ciphertext := append(nonce, dst...)
	// ciphertext := make([]byte, 12+len(dst))
	// copy(ciphertext[:12], nonce)
	// copy(ciphertext[12:], dst)

	// return string(ciphertext)
	return ciphertext
}
