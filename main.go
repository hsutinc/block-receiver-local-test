package main

import (
	"bytes"
	"compress/gzip"
	"crypto/sha1"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"block_receiver_local_test/schema"
	"block_receiver_local_test/tcp"
	"block_receiver_local_test/udp"
	"block_receiver_local_test/util"
)

const (
	assetDir             = "/Users/jasonchen/Workspace/Local_Test/asset_dir"
	fileDir              = assetDir + "/files"
	blockDir             = assetDir + "/blocks"
	subblockDir          = assetDir + "/subblocks"
	preprocessedBlockDir = assetDir + "/preprocessed_blocks"
)

func main() {

	// udp := flag.Bool("udp", false, "Enable udp transmission.")
	// tcp := flag.Bool("tcp", false, "Enable tcp transmission")
	udpSocketStr := flag.String("udp_socket", "", "UDP socket")
	tcpSocketStr := flag.String("tcp_socket", "", "TCP socket")

	fileM := make(map[string]int)
	blockM := make(map[int]int)

	flag.Func("files", "Key-value pairs of file name and assigned chunk number", func(args string) error {
		var err error
		for _, arg := range strings.Fields(args) {
			kv := strings.Split(arg, ":")
			chunkNum, serr := strconv.Atoi(kv[1])
			if serr != nil {
				err = serr
			}

			fileM[kv[0]] = chunkNum
			fmt.Println("key:", kv[0])
			fmt.Println("val:", kv[1])
		}
		return err
	})

	flag.Func("blocks", "Key-value pairs of file id and assigned chunk number", func(args string) error {
		var err error
		for _, arg := range strings.Fields(args) {
			kv := strings.Split(arg, ":")
			fileID, berr := strconv.Atoi(kv[0])
			if berr != nil {
				err = berr
			}

			chunkNum, cerr := strconv.Atoi(kv[1])
			if cerr != nil {
				err = cerr
			}

			blockM[fileID] = chunkNum
		}
		return err
	})

	flag.Parse()

	if *udpSocketStr != "" {
		udpC := udp.NewUDPClient(*udpSocketStr, subblockDir)
		if serr := udpC.SendBlocks(); serr != nil {
			fmt.Println(serr)
		}
	} else {
		fmt.Println("UDP socket not provided")
	}

	if *tcpSocketStr != "" {
		tcpC := tcp.NewTCPClient(*tcpSocketStr, preprocessedBlockDir)
		if serr := tcpC.SendBlocks(); serr != nil {
			fmt.Println(serr)
		}
	} else {
		fmt.Println("TCP socket not provided")
	}

	for filename, chunkNum := range fileM {
		chunkFile(filename, chunkNum)
	}

	for blockID, chunkNum := range blockM {
		chunkBlocksOfFile(blockID, chunkNum)
	}

	fmt.Println(fileM)
}

func chunkFile(filename string, chunkNum int) {
	fileFolder := fileDir + "/" + strings.TrimSuffix(filename, filepath.Ext(filename))
	filepath := fileFolder + "/" + filename
	fileIDStr, berr := ioutil.ReadFile(fileFolder + "/" + "file_id")
	if berr != nil {
		fmt.Println(berr)
	}

	fileID, serr := strconv.Atoi(string(fileIDStr))
	if serr != nil {
		fmt.Println(serr)
	}

	file, ferr := os.Open(filepath)
	if ferr != nil {
		fmt.Println(ferr)
		return
	}

	blockFolder := blockDir + "/" + fmt.Sprint(fileID)
	if merr := os.Mkdir(blockFolder, 0777); merr != nil {
		fmt.Println(merr)
	}

	processedBlockFolder := preprocessedBlockDir + "/" + fmt.Sprint(fileID)
	if perr := os.Mkdir(processedBlockFolder, 0777); perr != nil {
		fmt.Println(perr)
	}

	msg := fmt.Sprintf("file <%s> with file id <%d>", file.Name(), fileID)

	// chunk(file, chunkNum, blockFolder, msg)

	fileContent, rerr := ioutil.ReadFile(filepath)
	if rerr != nil {
		fmt.Println(rerr)
		return
	}

	blocks := util.ChunkData(fileContent, chunkNum /*blockFolder,*/, msg)

	checksum := getChecksum(fileContent)
	localPath := "../../aggregated_files"
	metadata := schema.MetadataFile{
		FileID:      fileID,
		OoyalaID:    fileID,
		FileName:    filename,
		LocalPath:   localPath,
		Size:        len(fileContent),
		ReleaseAt:   int(time.Now().UnixNano() / int64(time.Millisecond)),
		PublishAt:   int(time.Now().UnixNano() / int64(time.Millisecond)),
		TotalBlocks: chunkNum,
		Checksum:    checksum,
		ExecScripts: []string{
			fmt.Sprintf("mkdir -p %s", localPath),
			fmt.Sprintf("mv -f %s %s", filename, localPath),
		},
	}
	metadataByte, merr := json.Marshal(metadata)
	if merr != nil {
		fmt.Println("Failed marshalling metadata. err:", merr)
	}
	gzippedMetadata, gerr := gZipData(metadataByte)
	if gerr != nil {
		fmt.Println("Failed gzipping metadata. err:", gerr)
	}
	processedMetadata := util.Encrypt(appendBlockHeader(fileID, 0, gzippedMetadata))
	if err := ioutil.WriteFile(processedBlockFolder+"/0", processedMetadata, 0666); err != nil {
		fmt.Println("Failed writing metadata file. err:", err)
	}

	for i, block := range blocks {
		blockID := i + 1
		blockFileName := blockFolder + "/" + strconv.Itoa(blockID)
		processedBlockFileName := processedBlockFolder + "/" + strconv.Itoa(blockID)
		if _, werr := os.Create(blockFileName); werr != nil {
			fmt.Println(werr)
			continue
		}

		// write/save buffer to disk
		ioutil.WriteFile(blockFileName, block, 0666)
		processedBlock := util.Encrypt(appendBlockHeader(fileID, blockID, block))
		ioutil.WriteFile(processedBlockFileName, processedBlock, 0666)
	}
}

func chunkBlocksOfFile(fileID, chunkNum int) {
	preprocessedBlockFolder := preprocessedBlockDir + "/" + fmt.Sprint(fileID)

	blockEntries, rerr := os.ReadDir(preprocessedBlockFolder)
	if rerr != nil {
		fmt.Println(rerr)
	}

	r, _ := regexp.Compile(`^\.\w+`)

	for _, blockEntry := range blockEntries {
		if blockEntry.IsDir() || r.MatchString(blockEntry.Name()) {
			continue
		}

		blockIDStr := blockEntry.Name()

		blockID, cerr := strconv.Atoi(blockIDStr)
		if cerr != nil {
			fmt.Println(cerr)
		}

		data, ferr := ioutil.ReadFile(preprocessedBlockFolder + "/" + blockIDStr)
		if ferr != nil {
			fmt.Println(ferr)
		}

		subblockFolder := subblockDir + "/" + fmt.Sprint(fileID) + "/" + blockIDStr
		if merr := os.MkdirAll(subblockFolder, 0777); merr != nil {
			fmt.Println(merr)
		}

		msg := fmt.Sprintf("block <%d> with file id <%d>", blockID, fileID)

		subblocks := util.ChunkData(data, chunkNum /*subblockFolder,*/, msg)

		for i, subblock := range subblocks {
			subblockID := i + 1
			subblockFileName := subblockFolder + "/" + strconv.Itoa(subblockID)
			if _, werr := os.Create(subblockFileName); werr != nil {
				fmt.Println(werr)
				continue
			}

			processedSubblock := appendSubblockHeader(fileID, blockID, subblockID, chunkNum, subblock)
			// write/save buffer to disk
			ioutil.WriteFile(subblockFileName, processedSubblock, 0666)
		}
	}
}

func appendBlockHeader(fileID, blockID int, data []byte) []byte {
	processedData := make([]byte, 8+4+len(data))

	fileIDBinary := processedData[:8]
	binary.LittleEndian.PutUint64(fileIDBinary, uint64(fileID))
	blockIDBinary := processedData[8:12]
	binary.LittleEndian.PutUint32(blockIDBinary, uint32(blockID))

	copy(processedData[12:], data)

	return processedData
}

func appendSubblockHeader(fileID, blockID, subblockID, subblockNum int, data []byte) []byte {
	processedData := make([]byte, 8+4+1+1+len(data))

	fileIDBinary := processedData[:8]
	binary.LittleEndian.PutUint64(fileIDBinary, uint64(fileID))
	blockIDBinary := processedData[8:12]
	binary.LittleEndian.PutUint32(blockIDBinary, uint32(blockID))
	subblockIDBinary := processedData[12:13]
	copy(subblockIDBinary, []byte{byte(subblockID)})
	subblockNumBinary := processedData[13:14]
	copy(subblockNumBinary, []byte{byte(subblockNum)})

	copy(processedData[14:], data)

	return processedData
}

func getChecksum(data []byte) string {
	checksum := sha1.Sum(data)
	return hex.EncodeToString(checksum[:])
}

func gZipData(data []byte) (compressedData []byte, err error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)

	_, err = gz.Write(data)
	if err != nil {
		return
	}

	if err = gz.Flush(); err != nil {
		return
	}

	if err = gz.Close(); err != nil {
		return
	}

	compressedData = b.Bytes()

	return
}

func example() {
	var (
		urls  []string                    // Default of the empty slice
		pause time.Duration = time.Second // Default of one second
	)

	// The flag.Func() function takes three parameters: the flag name,
	// descriptive help text, and a function with the signature
	// `func(string) error` which is called to process the string value
	// from the command-line flag at runtime and assign it to the necessary
	// variable. In this case, we use strings.Fields() to split the string
	// based on whitespace and store the resulting slice in the urls
	// variable that we declared above. We then return nil from the
	// function to indicate that the flag was parsed without any errors.
	flag.Func("urls", "List of URLs to print", func(flagValue string) error {
		urls = strings.Fields(flagValue)
		return nil
	})

	// Likewise we can do the same thing to parse the pause duration. The
	// time.ParseDuration() function may throw an error here, so we make
	// sure to return that from our function.
	flag.Func("pause", "Duration to pause between printing URLs", func(flagValue string) error {
		var err error
		pause, err = time.ParseDuration(flagValue)
		return err
	})

	// Importantly, call flag.Parse() to trigger actual parsing of the
	// flags.
	flag.Parse()

	// Print out the URLs, pausing between each iteration.
	for _, u := range urls {
		// log.Println(u)
		fmt.Println(u)
		time.Sleep(pause)
	}
	fmt.Println(urls)
}
